provider "aws" {
}

variable "environment" {
  description = "Deployment Environment"
  type = string
}

variable "cost_center" {
  description = "Cost Center"
  type =  string
}

variable "subnet_config" {
  description = "CIDR Block & Name"
  type = list(object({
    cidr_block = string
    subnet_name = string
  }))
}

variable "vpc_config" {
  description = "CIDR & Name"
  type = list(object({
    cidr_block = string
    vpc_name = string
  }))
}

resource "aws_vpc" "ck-dev-vpc-1" {
  cidr_block = var.vpc_config[0].cidr_block
  tags = {
    Name = var.vpc_config[0].vpc_name
    Environment = var.environment
    Cost_Center = var.cost_center
  }  
}

resource "aws_subnet" "ck-dev-subnet-1" {
  vpc_id =  aws_vpc.ck-dev-vpc-1.id
  cidr_block = var.subnet_config[0].cidr_block
  availability_zone = "ap-south-1a"
  tags = {
    Name = var.subnet_config[0].subnet_name
    Environment = var.environment
    Cost_Center = var.cost_center
  }  
}

data "aws_availability_zones" "ck-dev-az" {
  state = "available"
}

resource "aws_subnet" "ck-dev-subnet-2" {
  vpc_id = aws_vpc.ck-dev-vpc-1.id
  cidr_block = var.subnet_config[1].cidr_block
  availability_zone = data.aws_availability_zones.ck-dev-az.names[1]
  tags = {
    Name = var.subnet_config[1].subnet_name
    Environment = var.environment
    Cost_Center = var.cost_center
  }
}

resource "aws_subnet" "ck-dev-subnet-3" {
  vpc_id =  aws_vpc.ck-dev-vpc-1.id
  cidr_block = var.subnet_config[2].cidr_block
  availability_zone = data.aws_availability_zones.ck-dev-az.names[2]
  tags = {
    Name = var.subnet_config[2].subnet_name
    Environment = var.environment
    Cost_Center = var.cost_center
  }
}